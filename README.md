# DingsBox

## Contribute

### New games / rituals

Add new games / rituals formatted in markdown into the corresponding directory:
`content/de/spiele|rituale/kategorien/<category>/titel.md`

Template files are located in `archetypes/titel.md`

### New categories

1. If you add a new category (e. g. of games) you may add the category and all the games inside the corresponding category directory
2. You need to create css styles for each of the categories in `/assets/css/main.css` and `/static/css/print.css`
3. You may create a small icon ressembling the category and save it to `/assets/images/"category".svg`

