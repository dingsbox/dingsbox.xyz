const form = document.querySelector('.print-mode');
form.addEventListener('change', function() {
    printSettings();
});


function printFunction() {
    window.print();
}

function printSettings() {
    var input = document.getElementById("print-mode");
    // Get HTML head element
    let head = document.getElementsByTagName('HEAD')[0];
    
    // Remove the old stylesheet
    let old = document.getElementById('printstyle');
    head.removeChild(old);
 
    // Create new link Element
    let link = document.createElement('link');
 
    // set the attributes for link element
    link.id = 'printstyle';
    link.rel = 'stylesheet';
    link.href = "/css/print-" + input.value + ".css";
    link.type = 'text/css';
 
    // Append link element to HTML head
    head.appendChild(link);
}

