function filterFunction() {
  // Declare variables
  var input, filter, ul, li, a, i, txtValue;
  input = document.getElementById('page-filter');
  filter = input.value.toUpperCase();
  ul = document.getElementById('pages');
  li = ul.getElementsByClassName('list');

  for (i = 0; i < li.length; i++) {
    a = li[i].getElementsByTagName('a')[0];
    txtValue = a.textContent || a.innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      li[i].style.display = "";
    } else {
      li[i].style.display = "none";
    }
  }
}

// Dark Mode Toggle

!function(){
    "use strict";
    var e=document.getElementById("light-theme"),t=document.getElementById("dark-theme"),c=document.body,a="light",o={
        light:"#f1eae4",dark:"#322e2f"
    }
    ;
    function n(n){
        a=n;
        var r=o[n];
        e.setAttribute("content",r),t.setAttribute("content",r),c.style.backgroundColor=r,function(e){
            localStorage&&localStorage.setItem("color-scheme",e)
        }
        (n)
    }
    function r(){
        var e=o.light,t=o.dark;
        o.dark=e,o.light=t
    }
    var d=window.matchMedia("(prefers-color-scheme: dark)");
    d.matches&&r(),d.addEventListener("change",(()=>{
        r(),n(a)
    }
    )),document.querySelectorAll('input[type=radio][name="theme"]').forEach((e=>e.addEventListener("change",(e=>{
        n(e.target.id)
    }
    )))),function(){
        if(localStorage){
            var e=localStorage.getItem("color-scheme");
            if(e)document.getElementById(e).checked=!0,n(e)
        }
    }
    ()
}
();
