#!/bin/sh

GAME_PAGE=./content/de/spiele/print.html
RITUAL_PAGE=./content/de/rituale/print.html

rm "$GAME_PAGE" "$RITUAL_PAGE"

preamble() {
    cat > "$1" << EOF
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        Spiele Druckvorlage
    </title>
    <script defer language="javascript" type="text/javascript"  src="/js/print.js"></script>
    <link rel="stylesheet" href="/css/print.css">
    <link rel="stylesheet" href="/css/fonts.css">
    <link id="printstyle" rel="stylesheet" href="/css/print-minimal.css" type="text/css">
</head>
<body>
    <div class="no-print">
        <h1>Druckansicht</h1>
        <p>
            Diese Seite kann über das Kontextmenu (Rechtsklick) oder mit Ctrl+P gedruckt werden.<br>
            Über das Druckmenu kann anschliessend die Kartengrösse mit der Funktion "Seiten pro Blatt" eingestellt werden.<br>
            So können die Karten schlussendlich einfach auseinander geschnitten werden.
        </p>
        <p>
            Die Druckeinstellung kann über das Menu (unten) angepasst werden.
            <ul>
                <li>Minimal: Keine Farben und keine Bilder (ähnlich wie diese Seite hier)</li>
                <li>Farbe: Mit Farben und kleinen Piktogrammen pro Kategorie</li>
                <li>Basisschrift: Wie "Farbe" aber in der Deutschschweizer Basisschrift</li>
            </ul>
        </p>
        <p>PS: Dieser Teil der Seite wird selbstverständlich nicht mitgedruckt.</p>
            <form>
                <label for="print-mode">Druckeinstellung:</label>
                <select class="print-mode" id="print-mode">
                  <option value="minimal">Minimal</option>
                  <option value="colored">Farbe</option>
                  <option value="full">Basisschrift</option>
                </select>
                <button id="print-now" onClick="print()">Drucken</button>
            </form>
    </div>

EOF
}

format() {
    INPUT_FILE="$1"
    CATEGORY="$2"
    OUTPUT_FILE="$3"


    TITLE=$(grep title "$INPUT_FILE" | sed -e 's/^title:\ //' -e "s/^[\'|\"]//" -e "s/[\"|\']$//")
    CONTENT=$(sed '1,/---/d' "$INPUT_FILE" | markdown | sed -e 's/^/\t\t\t/' -e 's/<hr \/>/<\/div>\n\t\t<hr class="colored-no-print">/')

    INFO_DIV="div class=\"$(echo "$CATEGORY" | tr '[:upper:]' '[:lower:]')\"" # 

    if [ "$(echo "$CONTENT" | wc -l)" -lt 28 ]; then
        PAGE_DIV="div class=\"page\""
    elif [ "$(echo "$CONTENT" | wc -l)" -lt 30 ]; then
        PAGE_DIV="div class=\"page smoll\""
    else
        PAGE_DIV="div class=\"page uilly-smoll\""
    fi
    
    if [ -f "./assets/images/$(echo "$CATEGORY" | tr '[:upper:]' '[:lower:]').svg" ]; then
        LOGO="<img class=\"logo\" src=\"/images/$(echo "$CATEGORY" | tr '[:upper:]' '[:lower:]').svg\">"
    else
        LOGO=""
    fi
    
    cat >> "$OUTPUT_FILE" << EOF
    <hr class="no-print">
    <hr class="no-print">
    
    <$PAGE_DIV>
        <div class="title">
            $LOGO
            <h2>$TITLE</h2>
            <h4><i>$CATEGORY</i></h4>
        </div>
        <hr class="colored-no-print">
        <$INFO_DIV>
$CONTENT
    </div>

EOF
}

appendix() {
    cat >> "$1" << EOF
</body>
<footer>
</footer>
</html>
EOF
}

preamble "$GAME_PAGE"

for i in ./content/de/spiele/kategorien/konzentrationsspiel/[!_]*.md
do
    format "$i" "Konzentrationsspiel" "$GAME_PAGE"
done

for i in ./content/de/spiele/kategorien/kooperationsspiel/[!_]*.md
do
    format "$i" "Kooperationsspiel" "$GAME_PAGE"
done

for i in ./content/de/spiele/kategorien/bewegungsspiel/[!_]*.md
do
    format "$i" "Bewegungsspiel" "$GAME_PAGE"
done

for i in ./content/de/spiele/kategorien/kennenlernspiel/[!_]*.md
do
    format "$i" "Kennenlernspiel" "$GAME_PAGE"
done

appendix "$GAME_PAGE"

preamble "$RITUAL_PAGE"

for i in ./content/de/rituale/kategorien/arbeitsstruktur/[!_]*.md
do
    format "$i" "Rituale" "$RITUAL_PAGE"
done

for i in ./content/de/rituale/kategorien/lebenszeitgliederung/[!_]*.md
do
    format "$i" "Rituale" "$RITUAL_PAGE"
done

for i in ./content/de/rituale/kategorien/zusammenleben/[!_]*.md
do
    format "$i" "Rituale" "$RITUAL_PAGE"
done

appendix "$RITUAL_PAGE"

