---
title: "Rituale"
description: "Hier findest du alle Rituale des DingsBox Projekts"
---

Da sich Kinder auf Strukturen und Rituale stützen, sollen auch ritualisierte Spiele in den Unterricht eingebettet werden.
Diese helfen einen routinierten Tagesablauf zu schaffen. 

Im Sonderpädagogischen Bereich wird häufig mit Ritualen oder klar vorgegebenen Tagesabläufen gearbeitet. 
Die Kinder wissen bereits vor Unterrichtsbeginn, was auf sie zukommt. 
Natürlich gibt es auch Ausnahmen, da im Alltag nicht immer alles nach Plan laufen kann, dann ist es jedoch wichtig, mit den Kindern klar zu kommunizieren. 

Auch in Regelklassen ist es förderlich, den Alltag zu strukturieren und Rituale einzubauen. 
Meist sind wir uns bereits vorhandenen Ritualen aber gar nicht bewusst. 
Nur schon das Feiern eines Geburtstages oder die Pausenzeiten im Alltag sind bereits Rituale.
