---
title: 'Zusammenleben Gestaltende'
description: 'Hier findest du alle Rituale der Kategorie "Zusammenleben Gestaltende"'
---

Rituale, die das Zusammenleben gestalten, helfen dabei miteinander in einer Art Gesellschaft zu leben und Konflike effizient zu lösen.
Dafür kann beispielsweise Klassenrat abgehalten und mit verschiedenen Kommunikationstechniken gearbeitet werden.
Sie beinhalten jedoch auch Dinge, wie das Verteilen von Ämtern und das Einhalten einer gesunden Feedbackkultur.
