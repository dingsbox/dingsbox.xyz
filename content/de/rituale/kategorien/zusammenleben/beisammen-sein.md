---
title: 'Beisammen Sein'
description: "Eine Lektion, um sich mit den Eltern auszutauschen und gemeinsam Zeit zu verbringen."
"rituale/kategorien":
    - Alle Rituale
    - Zusammenleben
"rituale/tags":
    - Eltern
    - Spezieller Anlass
"rituale/zeitaufwand":
    - eine Lektion
---

**Material:** Essen, Getränke

**Zeit:** Eine Lektion

---

**Beschreibung:**

Alle 2-3 Monate werden die Eltern an einem Nachmittag eingeladen, in die Schule zu kommen. 
Die Klasse bietet den Eltern verschiedene selbstgemachte Speisen (Gebäck) und Getränke (Limonade, Tee, Kaffee) an. 
Es dient als schöne Möglichkeit, mit den Eltern in Kontakt zu bleiben und Informationen auszutauschen, während die Kinder dabei sind. 

**Wichtig:** 

Die Termine sollten den Eltern so früh wie möglich mitgeteilt werden. 
Ein solches Beisammen sein kann auch nur innerhalb der Klasse und ohne Eltern stattfinden.
