---
title: 'Individuelle Rückmeldungen'
description: "Ein Beurteilungssystem, um Missverständnissen vorzubeugen."
"rituale/kategorien":
    - Alle Rituale
    - Zusammenleben
"rituale/tags":
    - Beurteilung
"rituale/zeitaufwand":
    - 10 min
---

**Zeit:**
    10 Minuten

---

**Beschreibung:**

Um Missverständnisse nach Prüfungen zu verhindern, bieten sich individuelle Rückmeldungen an. 
Auf der Vorderseite oder Rückseite befindet sich ein separates Fenster, bei dem die Lehrperson ihre individuelle Rückmeldung an das Kind verfassen kann. 
Zusätzlich kann bei Prüfungen auch eine kleine Selbstbeurteilung in Form von Smileys für Kinder integriert werden. 
Somit kann die LP den Schwierigkeitsgrad der Prüfung besser einschätzen und auch in der Rückmeldung darauf eingehen.
