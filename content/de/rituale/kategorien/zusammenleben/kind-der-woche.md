---
title: 'Kind Der Woche'
description: "Eine Methode, mit welcher jedes Kind von den anderen einmal nette Worte erhalten kann."
"rituale/kategorien":
    - Alle Rituale
    - Zusammenleben
"rituale/zeitaufwand":
    - 5 min
---

**Zeit:** 5 Minuten

---

**Beschreibung:**

Die SuS werden auf die Schulwochen aufgeteilt, sodass jedes Kind mindestens einmal das Kind der Woche ist. 
Zu Beginn der Woche teilt die LP den Kindern mit, welches Kind an der Reihe ist und visualisiert es, indem ein Bild des Kindes an die Wandtafel gehängt wird. 
Im Verlauf der Woche sollen alle anderen Kinder sich einmal die Zeit nehmen, dem Kind nette Worte oder ein Kompliment auf einen Papierstreifen zu schreiben und es in die Komplimente Box zu legen. 
Am Schluss der Woche darf das Kind der Woche die Komplimente lesen und mit nach Hause nehmen.
