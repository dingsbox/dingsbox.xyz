---
title: 'Klassenrat'
description: "Im Klassenrat werden Anliegen und Fragen besprochen."
"rituale/kategorien":
    - Alle Rituale
    - Zusammenleben
"rituale/zeitaufwand":
    - 20 min
---

**Material:** Hilfestellungen für das leitende Kind

**Zeit:** 20 Minuten

---

**Beschreibung:**

Dieses Ritual kann gut mit dem vorherigen Ritual «Kind der Woche» kombiniert werden. 
Es geht darum, dass sich die Kinder ihre Anliegen oder Fragen in den Klassenrat-Briefkasten werfen, um diesen während dem Klassenrat zu besprechen. 
Der Klassenrat kann von den Kindern selbst geleitet werden, indem die LP eine Anleitung vorgefertigt hat. 
Die Leitung kann beispielsweise vom Kind der Woche geführt werden. 
Der Klassenrat soll ein Wohlfühlort sein, bei dem alle Themen erlaubt sind und gemeinsam nach Lösungen gesucht werden kann.
