---
title: 'Morgenkreis'
description: "Eine Möglichkeit gemeinsam in den Tag zu starten."
"rituale/kategorien":
    - Alle Rituale
    - Arbeitsstruktur
"rituale/tags":
    - Tageseinstieg
"rituale/zeitaufwand":
    - 10 - 15 min
---

**Material:** Komplimentebox

**Zeit:** 10 - 15 Minuten

---

**Beschreibung:**

Jeden Morgen trifft sich die Klasse in einem Sitzkreis. 
Nach der Begrüssung wird den Kindern zuerst alles Bevorstehende mitgeteilt sowie den Tagesablauf erklärt. 
Danach bietet der Morgenkreis Raum für Fragen der Kinder. 

**Wichtig:** 

Auch hier gelten Regeln, die den Kindern bekannt sein müssen (leise sein, wenn jemand spricht, oder bei Fragen aufstrecken).
