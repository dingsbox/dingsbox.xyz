---
title: 'Aufmerksamkeitssignal'
description: "Ein akkustisches Signal hilft, die Aufmerksamkeit der SuS zu erhalten."
"rituale/kategorien":
    - Alle Rituale
    - Arbeitsstruktur
"rituale/tags":
    - Aufmerksamkeit
"rituale/zeitaufwand":
    - keiner
---

**Material:** Klangschale, Glocke etc.

---

**Beschreibung:**

Dieses Ritual hilft, den Kindern ohne Worte zu signalisieren, dass der Fokus nun auf der LP liegt. 
Mit einem klatschen oder einer Klangschale macht die LP auf sich aufmerksam. 
Dieses Ritual findet normalerweise mehrmals am Tag statt. 
Je öfter die LP den gleichen akustischen Ton durchführt desto schneller reagieren die Kinder.
