---
title: 'Special Helpers'
description: "Eine Möglichkeit, um den SuS Verantworung zu übertragen."
"rituale/kategorien":
    - Alle Rituale
    - Arbeitsstruktur
"rituale/tags":
    - Jobs
"rituale/zeitaufwand":
    - 10 min
---

**Material:** Visualisierung am Whiteboard

**Zeit:** 10 Minuten

---

**Beschreibung:**

Immer am Ende einer Woche werden die neuen «Special Helpers» bekannt gegeben. 
Diese wechseln sich gleichermassen ab. 
Die Special Helpers werden je nach Bedarf der Lehrperson eingesetzt zudem werden sie für verschiedene Bereiche eingeteilt wie z.B. Giessen der Pflanzen, Ordnung vor dem Schulzimmer, beim Bücherregal, Waschbecken, Farbstifte etc.
