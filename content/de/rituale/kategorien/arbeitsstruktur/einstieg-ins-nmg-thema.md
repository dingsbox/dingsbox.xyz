---
title: 'Einstieg Ins NMG-Thema'
description: "Zum Einstieg in ein neues Thema wird aus einer passenden Geschichte vorgelesen."
"rituale/kategorien":
    - Alle Rituale
    - Arbeitsstruktur
"rituale/tags":
    - Themeneinstieg
    - Vorlesen
"rituale/zeitaufwand":
    - 10 - 15 min
---

**Material:** Buch, Geschichte

**Zeit:** 10 - 15 Minuten

---

**Beschreibung:**

Passend zum NMG-Thema oder zur Jahreszeit kann die Lehrperson eine Vorlesesequenz mit einem passenden Buch gestalten. 
Im Sitzkreis liest sie den Kindern ca. 10 - 15 Minuten vor. 
Anschliessend können Fragen dazu beantwortet werden oder direkt am NMG-Thema weitergearbeitet werden. 
Wichtig ist, dass das Buch zur Alterstufe passt. 
Alternativ können auch Rätsel gemacht werden. 
Den Kindern wird dadurch ein spannender Einstieg ins Thema geboten, welches ca. 2x in der Woche stattfindet.
