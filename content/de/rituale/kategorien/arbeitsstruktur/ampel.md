---
title: 'Ampel'
description: "Eine Ampel wird als Mittel eingesetzt, um den Lärmpegel im Klassenzimmer zu steuern."
"rituale/kategorien":
    - Alle Rituale
    - Arbeitsstruktur
"rituale/tags":
    - Lautstärke
    - Unterricht
"rituale/zeitaufwand":
    - keiner
---

**Material:** Ampel

---

**Beschreibung:**

Die Ampel soll den SuS dabei helfen, den momentanen Lärmpegel zu erfassen und gleichzeitig anzupassen. 
Sie orientieren sich an den Farben der Ampel und wissen, welche Lautstärke erwünscht ist. 
Grün steht für Gruppenarbeiten und einem angepassten Unterhaltungston. 
Orange bedeutet Flüsterton und rot steht für stilles Arbeiten. 

Zu Beginn wird die Lehrperson die Kinder häufiger an die gewünschte Lautstärke erinnern müssen. 
Sobald die LP die Ampel aber genügend oft und konsequent eingesetzt hat, funktioniert es auch ohne zusätzliche Bemerkung der LP.
