---
title: 'Aufräumlied'
description: "Motivation zum Aufräumen mittels Musik verstärken."
"rituale/kategorien":
    - Alle Rituale
    - Arbeitsstruktur
"rituale/tags":
    - Motivation
"rituale/zeitaufwand":
    - 5 - 10 min
---

**Material:** Musik

---

**Beschreibung:**

5 - 10 Minuten bevor die Lektion endet, lässt die LP ein motivierendes Aufräumlied laufen. 
Solange das Lied läuft, haben die Kinder Zeit, still ihren Platz aufzuräumen. 
