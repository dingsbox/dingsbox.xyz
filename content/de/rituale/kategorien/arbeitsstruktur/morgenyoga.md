---
title: 'Morgenyoga'
description: "Eine Möglichkeit gemeinsam in den Tag zu starten."
"rituale/kategorien":
    - Alle Rituale
    - Arbeitsstruktur
"rituale/tags":
    - Tageseinstieg
"rituale/zeitaufwand":
    - 5 min
---

**Zeit:** 5 Minuten

---

**Beschreibung:**

Die Lehrperson führt mit den Kindern zusammen einfache Yogaübungen aus. 
Dies kann jeden Morgen vor dem Unterricht stattfinden. 
Die fliessenden Bewegungsabläufe im gleichmässigen Wechsel von Beugen und Strecken, sowie Ein- und Ausatmen unterstützt die Kinder darin, einen Rhythmus von Atmung und Bewegung zu finden.

**Wichtig:**

Je öfter dies mit den Kindern geübt wird, desto leichter fällt es den Kindern irgendwann. 
Sie lernen die Stille zu geniessen und nehmen ihren Körper wahr. 
Die LP soll den Kindern erklären, weshalb solche Übungen sinnvoll sind (Konzentration, Wahrnehmung etc.).
