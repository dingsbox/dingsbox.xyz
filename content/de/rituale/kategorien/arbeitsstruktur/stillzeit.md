---
title: 'Stillzeit'
description: "Eine Möglichkeit, um Unruhen zu verhindern."
"rituale/kategorien":
    - Alle Rituale
    - Arbeitsstruktur
"rituale/zeitaufwand":
    - 10 - 15 min
---

**Zeit:** 10 - 15 Minuten

---

**Beschreibung:**

In der Stillzeit sollen die Kinder jeweils ein Buch zur Hand nehmen, sich einen bequemen Platz suchen und für sich in Ruhe lesen oder sich das Buch anschauen. 
Die Stillzeit bietet sich nach der Morgenpause gut an, um wieder anzukommen. 
Somit entsteht nach der Pause keine Unruhe und alle wissen, was zu tun ist.
