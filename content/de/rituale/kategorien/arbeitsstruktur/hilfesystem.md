---
title: 'Hilfesystem'
description: "Ein System, das die SuS verfolgen können, sollten sie Hilfe benötigen."
"rituale/kategorien":
    - Alle Rituale
    - Arbeitsstruktur
"rituale/tags":
    - Hilfestellungen
"rituale/zeitaufwand":
    - 5 min
---

**Material:** Visualisierung mit Namen

**Zeit:** 5 Minuten

---

**Beschreibung:**

Sobald bei Arbeitsphasen Fragen auftauchen, sollen die Kinder ein System verfolgen, welches mit den Kindern zu Beginn besprochen wurde und zusätzlich an der Wand visualisiert wird.

1. Sich selbst helfen: Wo kann ich die Frage nachschauen? Welche Hilfsmittel habe ich?
2. Ein anderes Kind um Hilfe bitten: Kannst du mir mit meiner Frage weiterhelfen?
3. Hilfe der Lehrperson: Platziere dein Magnet am Whiteboard und arbeite an einer anderen Aufgabe, bis du an der Reihe bist. 

Sobald das Kind das Magnet platziert hat, geht die Lehrperson der Reihe nach durch. Das Kind geht dann mit der Frage zur Lehrperson und bekommt die benötigte Unterstützung. 

**Tipp:** 

Der Magnet ist mit dem Namen des Kindes beschriftet und verhindert so einem Auflauf von wartenden Kindern. 
Die LP kann so in Ruhe der Reihe nach die Fragen der Kinder beantworten.
