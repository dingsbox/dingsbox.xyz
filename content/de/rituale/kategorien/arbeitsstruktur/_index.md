---
title: 'Arbeit Strukturierende'
description: 'Hier findest du alle Rituale der Kategorie "Arbeit Strukturierende"'
---

Die Arbeit strukturierende Rituale bringen den Kindern Struktur und Routine in den Tagesablauf.
Dies beginnt mit der Begrüssung am Morgen und endet mit der Verabschiedung am Nachmittag.

Alles was in irgend einer Weise mit der Arbeit zu tun hat, kann durch solche Rituale gestützt werden.
Dazu gehören auch Kleinigkeiten wie das Aufräumen, die (Selbst-) Kontrolle von Arbeiten und Hausaufgaben sowie Ein- bzw. Ausstiege bei erweiterten Lehr- und Lernformen.
