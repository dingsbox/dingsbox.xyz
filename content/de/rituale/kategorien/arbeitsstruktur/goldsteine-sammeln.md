---
title: 'Goldsteine Sammeln'
description: "Ein Bonussystem, in welchem die Klasse als Ganzes Punkte sammelt."
"rituale/kategorien":
    - Alle Rituale
    - Arbeitsstruktur
"rituale/tags":
    - Bonussystem
"rituale/zeitaufwand":
    - keiner
---

**Material:** Silberne und goldene Steine

---

**Beschreibung:**

Es handelt sich um ein Bonussystem, bei dem die Kinder möglichst viele Silbersteine sammeln sollen. 
Wurden zehn Silbersteine erreicht, werden diese für einen goldenen eingetauscht. 
Die Lehrperson entscheidet, wie viele Goldsteine die Kinders sammeln müssen, um eine Belohnung zu bekommen. 
Die Kinder können beispielsweise für eine Abschlussreise mitbestimmen oder eigene Wünsche anbringen. 
Wichtig ist, dass die Klasse als Ganzes betrachtet werden sollte. 
Somit werden die Silbersteine auch nicht einem einzelnen Kind übergeben, sondern kommen immer in eine Schatzkiste, die der ganzen Klasse gehört.
