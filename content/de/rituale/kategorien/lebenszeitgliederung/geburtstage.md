---
title: 'Geburtstage: Würfelspiel'
description: "Eine Möglichkeit einen Geburtstag in der Klasse zu feiern."
"rituale/kategorien":
    - Alle Rituale
    - Lebenszeitgliederung
"rituale/tags":
    - Spezieller Anlass
"rituale/zeitaufwand":
    - 20 min
---

**Material:** Würfel

**Zeit:** 20 Minuten

---

**Beschreibung:**

Es gibt verschiedene Möglichkeiten einen Geburtstag zu feiern. 
Im Schulzimmer bietet sich aber ein Sitzkreis gut an. 
In die Mitte des Kreises kann ein kleiner Tisch stehen, der festlich und liebevoll geschmückt ist. 
Eine Möglichkeit ist dann das Würfelspiel. 
Das Geburtstagskind würfelt eine Zahl. 
Jede Zahl steht für eine andere Aktivität. (z. B. Lieder singen, Spiele, Vorlesen, Hörbücher, Tänze usw.)
    
Je nach Alterstufe kann die Leitung des Geburtstagsrituals auch in die Hände der SuS gegeben werden und somit stetig angepasst werden. 
Auch Wünsche und Anregungen sollte die LP von den Kindern einholen, um den Geburtstag des Kindes möglichst schön und individuell zu gestalten.
