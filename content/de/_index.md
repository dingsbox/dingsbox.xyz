---
title: "Das DingsBox Projekt"
description: "Hier findest du alles über das DingsBox Projekt."
---

Die DingsBox ist eine einfache Sammlung an Spielen, welche in kurzer Zeit und ohne grosse Vorbereitung gespielt werden können und Ritualen, welche helfen den Schulalltag der Kinder zu strukturieren.

Entstanden ist die DingsBox im Rahmen eines ALGE-Projekts an der Pädagogischen Hochschule St. Gallen und der Idee, eine Spielesammlung für Lehrpersonen erstellen zu wollen, die in verschiedenen Situationen zur Unterstützung eingesetzt werden kann.
Die Lehrperson soll mit der DingsBox in der Lage sein, schnellstmöglich ein passendes Spiel griffbereit zu haben, welches zudem pädagogische Ziele erfüllt. 
Wenn die Lehrperson beispielsweise das Klassenklima oder die sozialen Kompetenzen fördern möchte, wird sie in unserer Spielesammlung in der Kategorie "Kooperation" fündig.

Viel Spass beim Stöbern wünschen:

Aaron, Barbara und Corinne.

