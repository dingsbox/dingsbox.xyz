---
title: 'Mitwirken'
---

Möchtest du unser Projekt unterstützen und Lehrpersonen dabei helfen, eine stabile Sammlung an Spielen für zwischendurch und Ritualen zu schaffen, die für alle frei zugänglich ist?

Dann sende deine Vorschläge doch gerne an uns per [Mail](mailto://contact@azureorange.xyz) oder schlage direkt in unserem [GitLab Repo](https://gitlab.com/dingsbox/dingsbox.xyz) eine Änderung vor.

Vorlagen:

- [Spiele](/templates/spiele.md)

- [Rituale](/templates/rituale.md)
