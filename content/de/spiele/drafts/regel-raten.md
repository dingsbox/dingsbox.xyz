---
title: "Regel raten"
description: "Errate die Regel."
categories:
    - Aktivierungsspiel
tags:
    - Wörter
    - Raten
time:
    - 5 Minuten
groups:
    - alle zusammen
draft: true
---

- Die LP überlegt sich eine Regel:

  z. B. Wörter mit sechs Buchstaben und "a" am Schluss.

- Die SuS sagen Wörter und versuchen so die Regel herauszufinden.
