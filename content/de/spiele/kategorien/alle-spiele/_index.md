---
title: "Alle Spiele"
description: "Hier findest du alle Spiele des DingsBox Projekts"
---

Heutzutage haben viele Kinder Schwierigkeiten damit, sich in der Schule zu konzentrieren. 
Die Aufmerksamkeitsspanne liegt bei 7 – 10-jährigen Kindern bei ungefähr 20 Minuten, doch auch davon kann nicht immer ausgegangen werden. 
Vielen Kindern fällt es allgemein schwer, sich für eine längere Zeitspanne zu konzentrieren. 
Die Gründe dazu sind vielfältig: 
Es kann aufgrund fehlender Interessen oder ungünstigen Umständen wie Geräuschen, Gerüchen oder andere Reizen sein, aber auch Streitigkeiten untereinander können zu schlechter Konzentration führen. 
Auch seitens der Lehrperson kann die Konzentration beeinflusst werden, wenn sich die Kinder beispielsweise zu wenig unterstützt oder über- bzw. unterfordert fühlen. 
Das sind nur wenige Beispiele, die Gründe für ein unkonzentriertes Arbeiten aufzeigen.

Das DingsBox-Projekts hat zum Ziel, die Kinder in solchen Situationen wieder einzufangen und sie kurzzeitig von ihren Arbeiten zu lösen. 
Da es nicht immer sinnvoll wäre, die Kinder mit einem Konzentrationsspiel abzuholen, wenn die Konzentration schon längst verflogen ist, sind die Spiele nach verschiedenen Bedürfnissen kategorisiert. 
So dient die Spielesammlung den Lehrkräften als wertvolle Ressource, die jederzeit eingesetzt werden kann. 
Der Unterricht kann mit Spielen abwechslungsreicher gestaltet werden und bezieht die Schüler:innen aktiv ins Geschehen mit ein.

