---
title: "Genau hinhören"
description: "Kinder erraten von welchem Gegenstand das Geräusch kommt."
"spiele/kategorien":
    - Alle Spiele
    - Konzentrationsspiel
"spiele/tags":
    - Hören
"spiele/zeitaufwand":
    - 5 - 10 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Material:** Gegenstände im Schulzimmer 

**Zeit:** 5 - 10 Minuten 

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

LP läuft im Klassenzimmer umher und lässt Gegenstände ertönen.
Die SuS müssen erraten welcher Gegenstand gespielt wurde und Wiederholungen erkennen.
