---
title: 'Gegenstände Erfühlen'
description: "Die Habtische Wahrnehmung wird beim ertasten von Gegenständen auf die Probe gestellt."
"spiele/kategorien":
    - Alle Spiele
    - Konzentrationsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 5 - 10 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Material:**

- Gegenstände
- Tücher oder Säcke zum Verstecken

**Zeit:** 5 - 10 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Die Kinder sitzen im Stuhlkreis und schliessen die Augen. 
Mit den Händen müssen die Kinder Gegenstände ertasten und sie schliesslich korrekt benennen. 
Die Kinder dürfen beim Ertasten nicht reden.
