---
title: 'Farbenlesen'
description: "Ein Spiel, in dem Farb- und Worterkennung gegeneinander ausgespielt werden."
"spiele/kategorien":
    - Alle Spiele
    - Konzentrationsspiel
"spiele/tags":
    - Wörter
    - Farben
"spiele/zeitaufwand":
    - < 5 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Material:**

- vorbereitete PP-Päsentation / Video
- Konzentrationsübung "Verflixte Farben"

**Zeit:** 2 - 5 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Die Kinder müssen die Farbe und nicht das Wort so schnell wie möglich richtig sagen.
