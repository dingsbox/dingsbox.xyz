---
title: 'Inselspiel'
description: "Ein Denk- und Konzentrationsspiel, bei dem die Schüler eine geheime Regel herausfinden müssen, um mit auf die Insel zu dürfen."
"spiele/kategorien":
    - Alle Spiele
    - Konzentrationsspiel
"spiele/tags":
    - Logik
    - Muster erkennen
"spiele/zeitaufwand":
    - 5 - 10 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Material:** Keines

**Zeit:** 10 Minuten

**Gruppengrösse:** Alle zusammen

---

## **Spielbeschreibung**

Die Lehrperson beginnt mit einem Satz wie:
**"Mein Name ist Alex, ich gehe auf eine einsame Insel und nehme eine Ananas mit."**

Jeder Schüler nennt dann ebenfalls einen Gegenstand, den er mitnehmen möchte. Die Lehrperson antwortet mit:
"Ja, du darfst mit." oder "Nein, du darfst nicht mit."

Die geheime Regel bestimmt, wer mit darf.
Zum Beispiel: **Das gewählte Objekt muss mit dem gleichen Buchstaben beginnen wie der Name der Person.**

Das Spiel endet, wenn die meisten Schüler die Regel erkannt haben.

---

## **Varianten**

- **Wörter mit genau vier Buchstaben** (z. B. Buch, Tüte, Cola)
- **Das Wort muss einen bestimmten Buchstaben enthalten** (z. B. „A“)
- **Nur eine bestimmte Kategorie ist erlaubt** (z. B. Kleidung, Tiere, Lebensmittel, runde Gegenstände)
- **Das Wort muss genau drei Vokale haben** (z. B. Melone, Taschenuhr)
