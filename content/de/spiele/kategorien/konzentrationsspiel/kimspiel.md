---
title: 'Kimspiel'
description: "Änderungen in einer Formation erkennen."
"spiele/kategorien":
    - Alle Spiele
    - Konzentrationsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 10 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Material:** Verschiedene Gegenstände

**Zeit:** 10 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Die LP präsentiert eine Anzahl von Gegenständen in einer bestimmten Formation für 30 Sekunden. 
Anschliessend müssen die Kinder ihre Augen schliessen und die Lehrperson verändert einen oder zwei Gegenstände der Formation. 
Die Kinder müssen herausfinden, was geändert wurde. 
