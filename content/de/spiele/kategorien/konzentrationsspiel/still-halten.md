---
title: 'Still Halten'
description: "Ohne Unterbrechung versuchen, still zu sein."
"spiele/kategorien":
    - Alle Spiele
    - Konzentrationsspiel
"spiele/tags":
    - Wörter
    - Raten
    - Verstecken
"spiele/zeitaufwand":
    - < 5 min
"spiele/gruppengrösse":
    - alle zusammen
    - beliebig
---

**Zeit:** 1 - 5 Minuten

**Gruppengrösse:** keine oder in Gruppen als Wettbewerb

---

**Spielbeschreibung:**

Die Kinder müssen eine festgelegte Zeit still sein und ruhig sitzen. 
Das Spiel kann auch in Gruppen gespielt werden.
