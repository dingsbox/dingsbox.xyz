---
title: 'Wörter Zählen'
description: "Vorgegebene Wörter in einem gehörten Text zählen."
"spiele/kategorien":
    - Alle Spiele
    - Konzentrationsspiel
"spiele/tags":
    - Wörter
"spiele/zeitaufwand":
    - 5 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Material:** Text oder Geschichte

**Zeit:** 5 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Die LP liest den Kindern einen Text vor.
Zuvor werden ein oder zwei Wörter ausgesucht, die die Kinder zählen müssen.
Anschliessend liest die LP den Text vor und die Kinder müssen die besagten Wörter zählen.
Am Schluss wird aufgelöst. 
