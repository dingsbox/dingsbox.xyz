---
title: 'Fingerübung'
description: "Ein Geschicklichkeitsspiel, in welchem Fingerübungen nachgemacht werden."
"spiele/kategorien":
    - Alle Spiele
    - Konzentrationsspiel
"spiele/tags":
    - Geschicklichkeit
    - Geschwindigkeit
"spiele/zeitaufwand":
    - < 5 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Zeit:** 2 - 5 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Die LP zeigt den Kindern eine Fingerübung vor, bei der sich die Fingerkuppen gleichzeitig und abwechselnd berühren. 
Auf der rechten Seite beginnt man mit Daumen und kleinen Finger und auf der linken beginnt man mit dem Daumen und dem Zeigefinger. 
Die Kinder sollen die Übung korrekt und möglichst schnell ausführen können.
