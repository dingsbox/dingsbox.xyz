---
title: 'Wortkette'
description: "Zusammengesetzte Wörter aus einem Teil des zuvor genannten bilden."
"spiele/kategorien":
    - Alle Spiele
    - Konzentrationsspiel
"spiele/tags":
    - Wörter
"spiele/zeitaufwand":
    - 5 - 10 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Zeit:** 5 - 10 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Das erste Kind nennt ein Wort, dass aus zwei Wörtern besteht.
Das nächste Kind nimmt das zweite Wort des vorherigen Wortes und bildet ein neues Wort. 

Wichtig ist, dass sie gebildeten Wörter auch in der deutschen Sprache existieren.
