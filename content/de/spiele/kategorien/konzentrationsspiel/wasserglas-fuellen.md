---
title: 'Wasserglas Füllen'
description: "Ein Wasserglas möglichst füllen, ohne dabei hinzusehen."
"spiele/kategorien":
    - Alle Spiele
    - Konzentrationsspiel
"spiele/tags":
    - Hören
"spiele/zeitaufwand":
    - 10 - 15 min
"spiele/gruppengrösse":
    - alle zusammen
    - Kleingruppen
---

**Material:**

- Gläser / Becher
- Wasser
- Massbecher

**Zeit:** 10 - 15 Minuten

**Gruppengrösse:** Alle zusammen oder Kleingruppen

---

**Spielbeschreibung:**

Das erste Kind bekommt die Augen verbunden und muss das leere Glas mit dem Wasser aus dem Messbecher füllen. 
Hierbei sollte kein Wasser überlaufen. 
Nur durch die Geräusche, die das einlaufende Wasser macht, muss herausgefunden werden, wie voll das Glas ist. 
Weiter geht es reihum. 

Als Wettbewerb kann man das Spiel mit einer Stoppuhr ergänzen. 
