---
title: 'Achtung Konzentrieren'
description: "Ein Spiel, bei dem im Takt Begriffe einer bestimmten Kategorie genannt werden."
"spiele/kategorien":
    - Alle Spiele
    - Konzentrationsspiel
"spiele/zeitaufwand":
    - 5 - 10 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Zeit:** 5 - 10 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Die Kinder sitzen im Sitzkreis und patschen gemeinsam im vorgegebenen Takt der LP. 
Irgendwann sagt die LP: «Achtung, Konzentrieren» und nennt eine Kategorie wie z.B. Gemüsesorten. 
Die Kinder sollen nun reihum im Takt eine Sorte nennen. 
Wer nichts mehr weiss, scheidet aus.
