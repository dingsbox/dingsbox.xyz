---
title: 'Stille Post'
description: "Wörter oder Sätze so weitergeben, wie sie gehört werden."
"spiele/kategorien":
    - Alle Spiele
    - Konzentrationsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 5 - 10 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Zeit:** 5 - 10 Minuten

**Gruppengrösse:** alle zusammen

---

**Spielbeschreibung:**

Die Kinder sitzen im Sitzkreis. Ein Kind überlegt sich ein Wort oder einen Satz und flüstert dies dem Sitznachbarn ins Ohr. 
Wenn das Wort oder der Satz einmal die Runde gemacht, sagt das letzte Kind das Wort oder den Satz laut. 
