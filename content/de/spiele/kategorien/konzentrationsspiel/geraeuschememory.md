---
title: 'Geräuschememory'
description: "Gegenstände anhand des Klangs Paaren zuordnen und erkennen."
"spiele/kategorien":
    - Alle Spiele
    - Konzentrationsspiel
"spiele/tags":
    - Hören
"spiele/zeitaufwand":
    - 10 - 15 min
"spiele/gruppengrösse":
    - 2 - 3 P.
    - Kleingruppen
---

**Material:**
    
- Dosen
- Gegenstände in Paaren

**Zeit:** 10 - 15 Minuten

**Gruppengrösse:** 2 - 3 Personen

---

**Spielbeschreibung:**

Immer in zwei Dosen befindet sich jeweils der selbe Gegenstand.

Die Kinder müssen die Dosen schütteln und Paare finden, die sich gleich anhören. 
Anschliessend können die Gegenstände geratet werden.  
