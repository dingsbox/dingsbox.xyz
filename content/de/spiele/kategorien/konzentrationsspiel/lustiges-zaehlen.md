---
title: 'Lustiges Zählen'
description: "In der Gruppe zählen, ohne sich gegenseitig abzusprechen."
"spiele/kategorien":
    - Alle Spiele
    - Konzentrationsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 5 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Zeit:** 5 Minuten

**Gruppengrösse:** Alle Zusammen

---

**Spielbeschreibung:**

Ziel ist es, gemeinsam von 1 – 20 zu zählen, wobei die Kinder ihre Augen geschlossen haben. 
Eine Zahl darf nicht gleichzeitig gerufen werden, ansonsten muss von vorne begonnen werden. 
