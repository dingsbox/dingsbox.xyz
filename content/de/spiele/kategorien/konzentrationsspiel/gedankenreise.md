---
title: 'Gedankenreise'
description: "Die SuS lauschen einer Geschichte und entspannen sich dabei."
"spiele/kategorien":
    - Alle Spiele
    - Konzentrationsspiel
"spiele/tags":
    - Entspannung
    - Hören
    - Geschichten
"spiele/zeitaufwand":
    - 10 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Material:**

- Musik
- Geschichte

**Zeit:** 10 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Die Kinder suchen sich im Schulzimmer einen bequemen Ort, bei dem sie sich gut entspannen können und schliessen dabei die Augen. 
Das Zimmer wird abgedunkelt und die LP kann bei Bedarf eine ruhige Musik laufen lassen. 
Dabei liest sie den Kindern eine entspannende Geschichte vor.  
