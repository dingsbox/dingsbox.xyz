---
title: 'Konzentrationsspiele'
---

Konzentrationsspiele sind Aktivitäten, die im Unterricht vor allem dann eingesetzt werden, wenn die Kinder sich bei einer anschliessenden oder während einer Arbeit konzentrieren müssen. 
Diese Spiele haben das Ziel, die Aufmerksamkeit der Kinder zu erhöhen. 
Gerade bei längeren Unterrichtseinheiten können solche Spiele dazu beitragen, die SuS wieder neu zu motivieren, sodass sich die Lernbereitschaft steigert. 
Mit anregenden Aufgaben, die zum Rätseln oder Knobeln einladen, steigern sie die Konzentration der Kinder.
