---
title: 'Klammerketten'
description: "Blind eine Kette aus Büroklammern erstellen."
"spiele/kategorien":
    - Alle Spiele
    - Konzentrationsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 10 min
"spiele/gruppengrösse":
    - 2 P.
---

**Material:**

-Büroklammern
- Augenbinden

**Zeit:** 10 Minuten

**Gruppengrösse:** Zu zweit

---

**Spielbeschreibung:**

Alle Kinder bekommen die Augen verbunden und zu zweit zehn Büroklammern auf den Tisch gelegt. 
Ziel ist es, die Büroklammern blind aneinander zu reihen und so eine Kette aus Klammern zu erhalten. 
Wer dies schafft, darf laut fertig rufen und gewinnt das Spiel. 
