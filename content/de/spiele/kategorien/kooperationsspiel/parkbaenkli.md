---
title: 'Parkbänkli'
description: "Die Kinder, die auf der Parkbank sitzen improvisieren eine beliebige Szene."
"spiele/kategorien":
    - Alle Spiele
    - Kooperationsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 10 - 15 min
"spiele/gruppengroesse":
    - alle zusammen
---

**Material:** 3 Stühle

**Zeit:** 10 Minuten

**Gruppengrösse:** alle zusammen

---

**Spielbeschreibung:**

Die Kinder sitzen im Kreis. 
In der Mitte befinden sich 3 Stühle welche das sogenannte «Pärkli» darstellen. 
Das Spiel dient der Improvisation und regt die Kreativität an. 
In der Mitte dürfen immer drei Kinder sein und die Szene spielen, die ihnen gerade einfällt. 
Sie sollen Rücksicht auf die anderen Kinder nehmen, sodass alle einmal im Pärkli waren.
