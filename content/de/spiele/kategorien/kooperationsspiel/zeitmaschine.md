---
title: 'Zeitmaschine'
description: "In einer Gruppe Probleme aus der Zukunft oder Vergangenheit lösen."
"spiele/kategorien":
    - Alle Spiele
    - Kooperationsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 15 min
"spiele/gruppengroesse":
    - alle zusammen
---

**Zeit:** 15 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Die LP stellt die Kinder vor ein Problem, welches sich in der Zukunft abspielen könnte oder in der Vergangenheit geschah. 
Die Kinder müssen als Gruppe eine Zeitmaschine simulieren und gemeinsam Ideen entwickeln, um das Problem zu lösen. 
Als Alternative kann auch in Gruppen gespielt werden und die Geschichten bzw. Lösungen am Schluss vorgetragen werden. 
