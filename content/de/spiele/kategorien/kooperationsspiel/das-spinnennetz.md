---
title: 'Das Spinnennetz'
description: "Ohne Berührungen durch ein Netz aus Schnüren klettern."
"spiele/kategorien":
    - Alle Spiele
    - Kooperationsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 10 - 15 min
"spiele/gruppengroesse":
    - alle zusammen
---

**Material:** Schnur

**Zeit:** 10 - 15 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

In einem Bereich des Schulzimmers werden zwischen Bänken, Stühlen etc. viele Schnüre gespannt.
Die Kinder müssen nacheinander durch das Netz klettern, ohne es zu berühren oder zu zerstören. 

**Hinweis:**
Hier könnte man die Löcher der Schnüre mit Punkten versehen und es alt Wettbewerb spielen.
