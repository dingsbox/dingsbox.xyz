---
title: 'Kooperationsspiele'
---

Kinder lernen besser, wenn ein gutes Klassenklima vorhanden ist und sie gerne in die Schule gehen. 
Mit kooperativen Spielen lassen sich Teamgeist und soziale Kompetenzen fördern. 
Solche Spiele haben oft ein Problem, welches von der Klasse oder Gruppe als Gemeinschaft gelöst werden müssen. 
Die Kinder lernen dabei zu kommunizieren, zu beobachten und sich in der Gruppe anzupassen. 
Das Ziel liegt darin, eine Wertschätzung gegenüber den anderen in der Klasse zu entwickeln. 
Ihre sozialen und emotionalen Fähigkeiten werden dadurch gestärkt. 
Auch kann es dabei helfen, dass sich Kinder in Konfliktsituationen lösungsorientierter Verhalten. 
Die Zusammenarbeit in der Klasse ist ein wichtiger Bestandteil. 
Es soll von Beginn an mit kooperativen Spielen im Unterricht gearbeitet werden.

