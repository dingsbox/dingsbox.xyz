---
title: 'Alle Im Takt'
description: "Mehrere Bälle werden im Takt im Kreis weiteregegeben."
"spiele/kategorien":
    - Alle Spiele
    - Kooperationsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 5 - 10 min
"spiele/gruppengroesse":
    - alle zusammen
---

**Material:** 3 - 4 Tennisbälle

**Zeit:** 5 - 10 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Die SuS stehen in einem Kreis. 
Die Lehrperson steht auch im Kreis und hat 3 - 4 Tennisbälle vor sich liegen. 
Sie nimmt den ersten in die Hand und prallt ihn auf dem Boden, sodass ihn das Kind rechts fangen kann. 
Nun übergibt das zweite Kind den Ball auch «boden-auf» dem nächsten usw. 
Sobald der Ball bei einigen Kindern durch ist, startet die LP mit dem zweiten. 
Das Ziel ist es, dass schlussendlich alle Bälle im gleichen Takt weiteregegeben werden.
