---
title: 'Stange Balancieren'
description: "Zu zweit eine Stange, die nur mit einer Fingerspitze gehalten wird, möglichst rasch auf den Boden legen."
"spiele/kategorien":
    - Alle Spiele
    - Kooperationsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 10 min
"spiele/gruppengroesse":
    - 5 Personen
---

**Material:** Stange oder Meter

**Zeit:** 10 Minuten

**Gruppengrösse:** 5 Personen

---

**Spielbeschreibung:**

Die Kinder werden in Gruppen eingeteilt und bekommen jeweils eine Stange. 
Sie stehen nebeneinander in einer Linie und halten die Stande auf Bauchhöhe mit den beiden Zeigfingerspitzen. 
Das Ziel ist es, die Stange möglichst rasch auf dem Boden ablegen zu können. 
Die Finger dürfen sich von der Stange nie lösen.
