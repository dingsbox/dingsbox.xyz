---
title: 'König ruft ...'
description: "Alle Kinder zählen der Reihe nach im geschlagenen Puls. Die Kinder rufen einander an und versuchen zum Kaiser aufzusteigen."
"spiele/kategorien":
    - Alle Spiele
    - Kooperationsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 5 - 10 min
"spiele/gruppengroesse":
    - alle zusammen
---

**Zeit:** 5 - 10 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Die Kinder sitzen in einem Kreis. Der Reihe nach bekommen die Kinder die folgenden Namen: Kaiser, König, 1, 2, 3, 4 ...

Alle schlagen im vorgegebenen Puls zuerst auf die Oberschenkel und dann in die Hände. König beginnt und spricht im Puls "König ruft ..." und ruft somit jemand anderes auf. Dieses Kind ruft wieder jemanden auf. Sobald ein Kind nicht reagiert oder den Aufruf zurückgibt, muss es ans Ende der Schlange sitzen. Die anderen rutschen nach.

**Variationsmöglichkeiten**

Die Nummern können zur Erschwerung auch durch (mittelalterliche) Berufsgattungen ersetzt werden. Diese können nach sozialem Stand (von König bis Hofnarr) geordnet werden.
