---
title: 'Raupenlauf'
description: "Mit dem Bein des vorderen Kindes in der Hand versuchen alle gemeinsam, sich wie eine Raupe fortzubewegen."
"spiele/kategorien":
    - Alle Spiele
    - Kooperationsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 5 min
"spiele/gruppengroesse":
    - alle zusammen
---

**Zeit:** 5 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Alle stellen sich hintereinander in einer Reihe auf und jedes Kind nimmt ein Bein des Vorderen in die Hand. 
So versuchen sie sich anschliessend alle miteinander fortzubewegen. 
