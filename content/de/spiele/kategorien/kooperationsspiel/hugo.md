---
title: 'Hugo'
description: "Alle Kinder zählen der Reihe nach im geschlagenen Puls. Die Siebenerreihe wird dabei durch das Wort Hugo ersetzt."
"spiele/kategorien":
    - Alle Spiele
    - Kooperationsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 5 - 10 min
"spiele/gruppengroesse":
    - alle zusammen
---

**Zeit:** 5 - 10 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Die Kinder sitzen im Kreis. 
Alle schlagen im vorgegebenen Puls zuerst auf die Oberschenkel und dann in die Hände. 
Ein Kind beginnt im Rhythmus zu zählen. 
Das nächste Kind zählt im Rhythmus weiter. 
Alle Zahlen mit der Ziffer sieben und Zahlen aus der Siebenerreihe werden durch das Wort „Hugo“ ersetzt. 
Wer einen Fehler macht scheidet aus, oder muss ein Pfand abgeben.

**Variationsmöglichkeiten:**

- Die Richtung wechselt, je nachdem in welche Richtung die Person, die mit der Zahl an der Reihe ist, schaut.  
- Andere Ziffern und Reihen werden ausgewählt, zB. Ziffer sechs und Sechserreihe  
- Bei einem Fehler eine Aufgabe lösen und wieder einsetzen können, zB. Bewegungsaufgabe, Kopfrechnung lösen.  
