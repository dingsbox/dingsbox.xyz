---
title: 'Rollen Zu Zweit'
description: "Zu zweit wird versucht, ohne den Kontakt an den Füssen abzubrechen, über eine bestimmte Strecke zu rollen."
"spiele/kategorien":
    - Alle Spiele
    - Kooperationsspiel
"spiele/tags":
    - Wettkampf
"spiele/zeitaufwand":
    - 5 min
"spiele/gruppengroesse":
    - 2 Personen
---

**Zeit:** 5 Minuten

**Gruppengrösse:** 2 Personen

---

**Spielbeschreibung:**

Zwei Kinder liegen ausgestreckt auf dem Boden und berühren sich jeweils an den Füssen. 
Die beiden rollen nun seitwärts über eine gewisse Strecke bis ins Ziel und dürfen dabei nie den Kontakt an den Füssen abbrechen. 
Die schnellste Gruppe gewinnt dabei das Spiel. 

