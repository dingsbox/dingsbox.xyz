---
title: 'Komplimenteregen'
description: "Ein Kind wird von den anderen mit Komplimenten überschüttet."
"spiele/kategorien":
    - Alle Spiele
    - Kooperationsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 5 - 10 min
"spiele/gruppengroesse":
    - alle zusammen
---

**Zeit:** 5 - 10 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Ein Kind wird per Zufall ausgewählt und darf sich in die Mitte des Kreises stellen. 
Nun werden nach und nach die anderen Kinder ein Kompliment über das Kind in der Mitte regnen lassen, indem sie mit einem Tuch über das Kind fahren und das Kompliment aussprechen.
