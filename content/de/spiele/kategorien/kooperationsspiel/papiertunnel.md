---
title: 'Papiertunnel'
description: "Aus einem A4-Papier einen Kreis schneiden, durch welchen durchgegangen werden kann."
"spiele/kategorien":
    - Alle Spiele
    - Kooperationsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 10 min
"spiele/gruppengroesse":
    - 3 - 5 Personen
---

**Material:**

- Papier
- Schere

**Zeit:** 10 Minuten

**Gruppengrösse:** 3 - 5 Personen

---

**Spielbeschreibung:**

Die Klasse wird in Gruppen aufgeteilt. 
Jede Gruppe geht in eine andere Ecke des Zimmers. 
Pro Gruppe gibt es ein A4-Papier und eine Schere. 
Die SuS bekommen folgenden Auftrag: Versucht innerhalb der Zeit (ca. 3-5’) aus dem Papier einen möglichst grossen Kreis zu schneiden, damit am Schluss die ganze Gruppe gemeinsam durch den Kreis steigen kann. 
Der Kreis darf keinen Anfang und kein Ende haben.
