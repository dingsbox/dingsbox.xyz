---
title: 'Stühle Kippen'
description: "Sich auf einem Bein als Gruppe um einen Kreis gekippter Stühle bewegen."
"spiele/kategorien":
    - Alle Spiele
    - Kooperationsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 5 - 10 min
"spiele/gruppengroesse":
    - alle zusammen
---

**Material:** Stühle

**Zeit:** 5 - 10 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Die Stühle werden im Kreis mit der Sitzfläche nach innen aufgestellt. 
Jedes Kind stellt sich aussen an einen Stuhl, alle mit Blick in dieselbe Richtung. 
Nun kippen die Kinder den Stuhl zu sich, halten ihn oben mit der Hand fest und stehen auf einem Bein. 
Das Ziel ist es nun sich als Gruppe fortzubewegen, indem die Stühle gekippt bleiben und die Kinder immer auf einem Bein stehenbleiben müssen.
