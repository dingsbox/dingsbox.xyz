---
title: 'Gordischer Knoten'
description: "1 - 2 Kinder versuchen die anderen, ineinander verknoteten Kinder voneinander zu lösen."
"spiele/kategorien":
    - Alle Spiele
    - Kooperationsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 10 min
"spiele/gruppengroesse":
    - alle zusammen
    - 5 - 10 Personen
---

**Zeit:** 10 Minuten

**Gruppengrösse:** Alle zusammen / Gruppen von ca. 6 Personen

---

**Spielbeschreibung:**

1 - 2 Kinder verlassen das Schulzimmer und warten. 
Die anderen Kinder stehen in einem Kreis und beginnen sich zu verknoten und zu verhaken, sodass ein möglichst grosses Durcheinander entsteht. 
Nun holt die LP das Kind vor dem Schulzimmer herein, welches den Knoten möglichst rasch lösen soll, indem es sagt welches Kind was machen soll.

