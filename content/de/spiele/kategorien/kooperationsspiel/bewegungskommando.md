---
title: 'Bewegungskommando'
description: "Mittels Ansagen der Gruppe und verbundenen Augen durch einen Parkour gehen."
"spiele/kategorien":
    - Alle Spiele
    - Kooperationsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 10 min
"spiele/gruppengroesse":
    - 4 - 5 Personen
---

**Material:** Klebeband

**Zeit:** 10 Minuten

**Gruppengrösse:** 4 - 5 Personen

---

**Spielbeschreibung:**

Die Kinder werden in Gruppen eingeteilt und bestimmen eine Person, die sich die Augen verbinden lässt. 
In der Zwischenzeit kleben die anderen Mitspieler einen Parkour mit Klebeband. 
Die Person mit den verbundenen Augen wird nun durch den Parkour mittels Ansagen der anderen geführt. 
Die Ansagen sollen möglichst klar sein, sodass die Person mit den verbundenen Augen den Parkour möglichst schnell und ohne Fehler meistern kann. 

**Hinweis für die LP:**
Dieses Spiel ist eine gute Möglichkeit, die sozialen Kompetenzen zu fördern. 
Die Kinder sollen innerhalb der Gruppe alle Aufgaben verteilen, sodass das Spiel möglichst ohne Störungen und Zwischenfälle verläuft. 
Hier kann die LP das Verhalten und die Rollen der SuS beobachten.
