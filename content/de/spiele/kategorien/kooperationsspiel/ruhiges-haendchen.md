---
title: 'Ruhiges Händchen'
description: "Mit Schnüren einen Stift bewegen und versuchen ein Mustern nachzufahren."
"spiele/kategorien":
    - Alle Spiele
    - Kooperationsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 10 min
"spiele/gruppengroesse":
    - 5 Personen
---

**Material:**

- Schnur
- Stift
- A3-Papier mit einem vorgezeichneten Muster

**Zeit:** 10 Minuten

**Gruppengrösse:** 5 Personen

---

**Spielbeschreibung:**

Die SuS werden in Gruppen eingeteilt. 
Sie erhalten alle ein A3 Papier mit einem vorgezeichneten Gesicht. 
Die Kinder sitzen in einem Kreis. 
In der Mitte liegen das A3 Papier und Schnüre, die an einen dicken Stift gebunden wurden. 
Das Ziel ist es, das Gesicht möglichst genau nachzufahren, indem die Kinder nur durch das Ziehen und Loslassen ihrer Schnüre den Stift in Bewegung und auf das Blatt bringen. 
Hierbei ist Feingefühl gefragt. 
Die Kinder sollen dabei nicht reden.
