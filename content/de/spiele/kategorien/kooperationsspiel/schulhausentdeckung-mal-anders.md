---
title: 'Schulhausentdeckung Mal Anders'
description: "Blind einen Weg durchs Schulhaus gehen und diesen anschliessend auf Papier aufzeichnen."
"spiele/kategorien":
    - Alle Spiele
    - Kooperationsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 10 - 15 min
"spiele/gruppengroesse":
    - 2 Personen
---

**Zeit:** 10 - 15 Minuten

**Gruppengrösse:** 2 Personen

---

**Spielbeschreibung:**

Die Kinder werden in Zweiergruppen eingeteilt. 
Ein Kind der Zweigruppe bekommt die Augen verbunden. 
Nun stellen sich alle Zweiergruppen in einer Reihe im Schulzimmer auf. 
Die Zweiergruppen halten sich jeweils am Arm oder an den Händen. 
Nun läuft die Lehrperson voraus und führt die Gruppen ca. 3’ durchs Schulhaus. 
Die Kinder, die etwas sehen, dürfen nicht sagen, wo sie langgehen. 
Sie geben lediglich Anweisungen zu Treppen oder anderen Hürden. 
Zurück im Schulzimmer angekommen, sollen die Kinder mit der Augenbinde ihren Weg auf ein Papier aufzeichnen und versuchen die Strecke zu rekonstruieren. 
Dann wird aufgelöst und die Rollen getauscht.
