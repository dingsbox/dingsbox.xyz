---
title: 'Die Gemeinsame Skulptur'
description: "Einen möglichst hohen und stabilen Turm errichten."
"spiele/kategorien":
    - Alle Spiele
    - Kooperationsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 10 - 15 min
"spiele/gruppengroesse":
    - 4 - 5 Personen
---

**Material:** Baumaterial / Bauklötze

**Zeit:** 10 - 15 Minuten

**Gruppengrösse:** 4 - 5 Personen

---

**Spielbeschreibung:**

Die Klasse wird in Gruppen eingeteilt und verteilt sich im Schulzimmer. 
Vor jeder Gruppe befindet sich eine bestimmte Anzahl an Bastelmaterial und / oder Bauklötzen. 
Das Ziel der Gruppen ist es, in bestimmter Zeit eine Skulptur zu bauen, die möglichst stabil und hoch ist.
