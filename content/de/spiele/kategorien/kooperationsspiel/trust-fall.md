---
title: 'Trust Fall'
description: "Das Vertrauen entwickeln, sich in die Arme von jemand anderem fallen zu lassen."
"spiele/kategorien":
    - Alle Spiele
    - Kooperationsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 5 - 10 min
"spiele/gruppengroesse":
    - 5 Personen
---

**Zeit:** 5 - 10 Minuten

**Gruppengrösse:** 5 Personen

---

**Spielbeschreibung:**

Die SuS werden in Gruppen eingeteilt. 
Nach und nach soll sich eine Person trauen, sich in die Hände und Arme der anderen Kinder fallen zu lassen. 
Wichtig ist die Körperspannung die Konzentration der anderen Kinder. 
Wenn sich eine Gruppe wohl fühlt, kann der "Trust Fall" auch beispielsweise von einem Tisch aus stattfinden.  

**Hinweis:**
Die Gruppen sollen zufällig gewählt werden und es soll auf eine klare Anweisung geachtet werden. 
Die Kinder müssen konzentriert und aufmerksam sein, sodass keine Unfälle passieren. 
Mit dieser Übung wird das Vertrauen der Kinder gestärkt. 
