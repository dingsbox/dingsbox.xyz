---
title: 'Superlativ'
description: "Hier eine kurze Beschreibung des Spieles einfügen."
"spiele/kategorien":
    - Alle Spiele
    - Kennenlernspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 5 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Zeit:** 5 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Alle Schüler:innen überlegen sich etwas, dass sie sehr gut können. 
Anschliessend darf jedes Kind der Reihe nach seinen / ihren Namen sagen und eine zusätzliche Fähigkeit, welche sie besonders gut beherrschen. 

z.B. Ich heisse Philipp und kann am besten Tanzen, ich heisse Lukas und trinke am liebsten Rivella.
