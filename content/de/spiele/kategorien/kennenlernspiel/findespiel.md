---
title: 'Findespiel'
description: "Hier eine kurze Beschreibung des Spieles einfügen."
"spiele/kategorien":
    - Alle Spiele
    - Kennenlernspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 15 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Material:** zerschnittene Postkarten

**Zeit:** 15 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Es werden Postkarten in 4 - 5 Teile zerschnitten und an die Kinder verteilt. 
Die Schüler:innen suchen nun die anderen Kinder mit Teilen aus der gleichen Karte. 
So werden Gruppen gebildet und die Kinder unterhalten sich über vorgegebene Fragen (z.B. wie waren deine Ferien, was war dein spannendstes Erlebnis, etc.).
