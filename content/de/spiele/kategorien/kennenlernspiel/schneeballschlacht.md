---
title: 'Schneeballschlacht'
description: "Hier eine kurze Beschreibung des Spieles einfügen."
"spiele/kategorien":
    - Alle Spiele
    - Kennenlernspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 20 - 30 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Zeit:** 10 + 20 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Ca. 10 Minuten werden benötigt, damit sich die Kinder Fakten über sich überlegen können.

Alle Schüler/-innen schreiben drei Aussagen über sich auf ein Blatt Papier. 
Danach knüllen alle ihr Blatt zusammen und machen damit für etwa eine Minute eine Schneeballschlacht. 
Anschließend hebt jeder einen Schneeball auf und muss nun die Person finden, die sich darauf beschrieben hat. 
Am Schluss stellt jeder seinen Partner bzw. seine Partnerin der Klasse vor und berichtet, was er Neues über sie bzw. ihn erfahren hat.
