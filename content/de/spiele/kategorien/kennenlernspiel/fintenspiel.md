---
title: 'Fintenspiel'
description: "Hier eine kurze Beschreibung des Spieles einfügen."
"spiele/kategorien":
    - Alle Spiele
    - Kennenlernspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 15 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Material:** Notizzettel

**Zeit:** 15 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Alle Schüler:innen schreiben drei Sätze oder Geschichten auf. 
Zwei der Aussagen entsprechen der Wahrheit und eine Geschichte ist gelogen. 
Anschliessend werden die drei Sätze vorgelesen und die anderen Kinder versuchen nun die falsche Aussage zu erraten. 
Dann werden die Aussagen geklärt. 
Die Lehrperson hat nun wieder die Möglichkeit über die korrekten Geschichten noch mehr Informationen einzuholen.
