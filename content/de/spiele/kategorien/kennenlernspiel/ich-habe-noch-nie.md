---
title: 'Ich Habe Noch Nie ...'
description: "Hier eine kurze Beschreibung des Spieles einfügen."
"spiele/kategorien":
    - Alle Spiele
    - Kennenlernspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 15 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Material:** Beispielfragen, um bei Bedarf zu helfen.

**Zeit:** 15 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Alle Kinder sitzen im Kreis, ausser einer Person, die in der Mitte steht. 
Das Kind in der Mitte sagt etwas, was er oder sie noch nie gemacht hat. 
Alle anderen Kinder, die dies ebenfalls noch nie getan haben, stehen auf und setzen sich auf einen anderen Platz. 
Wer keinen Platz bekommen hat, muss in die Mitte. 
Einige Kinder könnten sich aber schwertun, sich Sätze mit „Ich habe noch nie …“ einfallen zu lassen. 
Dann können Sie Vorschläge einbringen, wie:

- Ich habe noch nie einen Kaugummi verschluckt.
- Ich habe noch nie Urlaub ohne meine Eltern gemacht.
- Ich habe noch nie Krokodilfleisch gegessen.

Die Sätze können auch abgewandelt werden in „Ich bin noch nie …“ oder „Ich war noch nie …“, 

- Ich bin noch nie geflogen.
- Ich bin noch nie vom Fahrrad gefallen.
- Ich war noch nie auf einem Kreuzfahrtschiff. 
- Ich war noch nie in Afrika.
- Ich war noch nie auf der Zugspitze.
