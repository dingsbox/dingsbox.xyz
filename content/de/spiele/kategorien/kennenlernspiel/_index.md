---
title: 'Kennenlernspiele'
---

Das Kennenlernen von neuen Leuten nimmt immer etwas Zeit in Anspruch.
Mit einem kleinen Spiel kann dieser Prozess lustig gestaltet werden und vielfach kann man sich auch die Namen der neuen Personen schneller merken.
Aus diesem Grund finden sich hier in der Kategorie der Kennenlernspiele, verschiedene kleine Spiele, mit deren Hilfe neue Leute schnell kennengelernt werden können.
Besonders zu empfehlen sind diese Spiele, wenn eine Lehrperson in eine neue Klasse kommt oder wenn ein neues Kind in die Klasse kommt.
