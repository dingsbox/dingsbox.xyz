---
title: 'Lebende Statistik'
description: "Hier eine kurze Beschreibung des Spieles einfügen."
"spiele/kategorien":
    - Alle Spiele
    - Kennenlernspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 20 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Material:** Die Lehrperson muss ca. 10 Minuten einplanen, um sich verschiedene Fragen zu überlegen.

**Zeit:** 20 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Die Lehrperson sucht sich ein Kriterium aus, nach welchem sich die Kinder der Reihe nach aufstellen. 
Z. B. der Name nach dem Alphabet, der Geburtsmonat, die Körpergrösse, Anzahl Geschwister etc. 
Die Kinder tauschen sich nun untereinander aus und versuchen als Klasse die Aufgabe fehlerfrei zu lösen. 
Die Lehrperson kann am Schluss die korrekte Aufstellung mit Kontrollfragen überprüfen.
