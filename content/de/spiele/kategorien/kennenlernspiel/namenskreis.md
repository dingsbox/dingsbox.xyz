---
title: 'Namenskreis'
description: "Hier eine kurze Beschreibung des Spieles einfügen."
"spiele/kategorien":
    - Alle Spiele
    - Kennenlernspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 10 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Zeit:** 10 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Alle Schüler:innen stellen sich im Kreis auf und nennen der Reihe nach ihren Namen (falls nicht schon bekannt). 
Bei diesem Spiel ist es wichtig, dass jeder Name nur einmal vorkommt. 
Bei zwei oder mehreren gleichen Namen müssen Spitznamen verwendet werden. 
Ein Kind steht nun in die Mitte und die anderen bleiben stehen. 
Die Person in der Mitte nennt nun einen Namen und die genannte Person nennt schnell einen neuen Namen. 
Das Kind in der Mitte muss versuchen die genannte Person zu berühren, bevor diese einen weiteren Namen nennen kann. 

Das Spiel kann auch erweitert werden, indem Knaben Mädchen aufrufen müssen oder der Sitznachbar nicht genannt werden darf. 
So wird verhindert, dass sich immer nur Freunde gegenseitig aufrufen.
