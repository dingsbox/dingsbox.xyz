---
title: 'Planetenspiel'
description: "Hier eine kurze Beschreibung des Spieles einfügen."
"spiele/kategorien":
    - Alle Spiele
    - Kennenlernspiel
"spiele/tags":
"spiele/zeitaufwand":
    - < 5 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Material:** mind. 1 - 2 verschiedene Bälle

**Zeit:** 5 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Alle Spieler stellen sich in einem Kreis auf.
Die erste Person beginnt und wirft den Ball zu einer anderen, welche anschliessend den eigenen Namen nennt.
So macht der Ball die Runde durch den Kreis, bis alle den Ball einmal hatten und ihren Namen genannt haben.

In einer zweiten Runde, verfolgt der Ball die selbe Route, doch wird nun der Name der adressierten Person anstelle des eigenen genannt.

**Erweiterung**

Es können weitere Bälle eingeführt werden, die eine andere Route beschreiben.
Weiter können alle Bälle miteinander die Runde machen und es kann geschaut werden, welcher Ball schneller wieder am Ausgangspunkt angelangt.
