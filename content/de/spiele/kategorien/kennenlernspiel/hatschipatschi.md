---
title: 'Hatschipatschi'
description: "Hier eine kurze Beschreibung des Spieles einfügen."
"spiele/kategorien":
    - Alle Spiele
    - Kennenlernspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 10 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Zeit:** 10 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Alle Schüler:innen sitzen in einem Stuhlkreis. 
Ein Kind geht nach draussen und während ihrer / seiner Abwesenheit wird ein Hatschipatschi bestimmt und ein Stuhl entfernt. 
Die Person kommt wieder herein und steht in die Mitte des Kreises. 
Sie zeigt auf verschiedene Kinder, welche ihren Namen nennen. 
Sobald die Person in der Mitte auf den Hatschipatschi zeigt, sagt dies «Hatschipatschi» anstelle des Namens und es wird so schnell wie möglich versucht einen anderen Stuhl zu ergattern. 
Dasjenige Kind, welches keinen Stuhl erwischt, geht nun nach draussen. 
(Achtung bei tieferen Stufen bleiben die Kinder gerne absichtlich stehen um nach draussen zu dürfen. 
In diesem Fall ist es sinnvoll, dass die Lehrperson bestimmt, wer nach draussen darf). 

**Varianten**

Das Spiel kann auch (gerade für die Mittelstufe) noch erweitert werden. 
Anstatt dem eigenen Namen sagen die Kinder nun zwei im Vorhinein bestimmte Kriterien (z.B. Name des Vaters und das Lieblingstier). 
Das Kind im Kreis muss nun versuchen das Rätsel herauszufinden. 
Die Auflösung der zwei Begriffe erfolgt nach dem Stuhlwechsel.
