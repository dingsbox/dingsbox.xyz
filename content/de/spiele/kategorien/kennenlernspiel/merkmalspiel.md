---
title: 'Merkmalspiel'
description: "Hier eine kurze Beschreibung des Spieles einfügen."
"spiele/kategorien":
    - Alle Spiele
    - Kennenlernspiel
"spiele/zeitaufwand":
    - 10 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Material:** verschiedene Merkmale aufschreiben

**Zeit:** 10 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Alle Schüler:innen setzen sich in den Kreis. Die Lehrperson hat verschiedene Merkmale vorbereitet. Z.B. „Ich habe ein Haustier zuhause, ich habe Geschwister, ich mache gerne Sport“. Die Kinder, auf welche die jeweilige Aussage der Lehrperson zutrifft, stehen auf. Nun hat die Lehrperson die Gelegenheit bei den Kindern nach etwas mehr Details nachzufragen. Kinder erzählen in der Regel sehr gerne und ausführlich, deshalb muss darauf geachtet werden, dass alle Kinder (die es wünschen) zu Wort kommen. 
Das Spiel kann auch mit verschiedenen Bewegungen durchgeführt werden. Anstatt nur aufzustehen können die Kinder einen Hampelmann machen oder um den Stuhl laufen.
