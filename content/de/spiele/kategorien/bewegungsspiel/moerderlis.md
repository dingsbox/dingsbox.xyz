---
title: 'Mörderlis'
description: "Finde den Mörder bevor alle Kinder weggeblinzelt wurden."
"spiele/kategorien":
    - Alle Spiele
    - Bewegungsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 5 - 10 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Zeit:** 5 - 10 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Die Klasse bildet einen Kreis. 
Ein Schüler (Detektiv) verlässt das Zimmer. 
Die übrigen Schüler bestimmen einen Mörder. 
Der Detektiv betritt das Zimmer. 
Der Mörder beginnt nun einen Schüler nach dem anderen durch Zuzwinkern umzubringen. 
Die umgebrachten Schüler gehen zu Boden. 
Der Detektiv muss erraten, wer der Mörder ist.
