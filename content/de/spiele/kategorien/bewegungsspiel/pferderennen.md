---
title: 'Pferderennen'
description: "Klatsche ein Pferderennen und folge den Anweisungen."
"spiele/kategorien":
    - Alle Spiele
    - Bewegungsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 5 - 10 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Zeit:** Eine Minute

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Alle TN bilden einen Kreis. 
Alle gehen in die Knie und klatschen mit den Händen auf die Oberschenkel. 
Die Pferde (Hände) gehen an den Start, nach dem Count-down starten alle Pferde. 
Alle klatschen so schnell wie möglich auf die Oberschenkel. 

Die Spielleitung gibt verschiedene Anweisungen:

- Links/rechts Kurve: Alle neigen sich nach links/rechts.
- Hürde/doppelte Hürde: Alle TN springen einmal/zweimal in die Luft
- Applaus des Publikums: Alle TN klatschen
- Wassergraben: Alle streichen sich mit den Fingernüber die Lippe und machen Geräusch eines Wassergrabens.
- Fotografen: alle halten pantomimisch eine Kamera in der Hand und machen Fotos.
