---
title: 'Stiller Spaziergang'
description: "Gehe achtsam durch den Raum und merke dir die Positionen der anderen."
"spiele/kategorien":
    - Alle Spiele
    - Bewegungsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 10 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Zeit:** 10 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Die Schülerinnen und Schüler gehen ruhig und langsam im Klassenzimmer umher und versuchen dabei auf alle anderen Kinder zu achten. 
Wenn die Lehrperson einen Namen nennt, schliessen die Kinder die Augen und zeigen auf das genannte Kind. 
Wenn alle bereit sind, werden die Augen geöffnet und es wird kontrolliert, ob die Kinder in die Richtung des genannten Kindes zeigen. 
Dies kann für mehrere Runden wiederholt werden. 
Das Spiel soll beruhigend wirken und den Kindern dabei helfen sich neu zu fokussieren und die Ruhe ins Schulzimmer zurückzubringen.
