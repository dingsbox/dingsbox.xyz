---
title: 'Dirigentenspiel'
description: "Finde den Dirigenten in einem Orchester von Körperperkussionisten."
"spiele/kategorien":
    - Alle Spiele
    - Bewegungsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 5 - 10 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Zeit:** 5 - 10 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Die Gruppe sitzt im Kreis, ein Mitspieler verlässt den Raum. 
Die übrige Gruppe bestimmt nun, wer Dirigent oder Dirigentin sein soll. 
Seine Aufgabe ist es, möglichst unauffällig den anderen Zeichen zu geben, wann sie ein anderes Instrument spielen sollen. 
Dann kommt der Spieler, der draußen gewartet hat, zurück in den Raum. 
Die Gruppe beginnt pantomimisch ein Instrument zu spielen. 
Nach kurzer Zeit muss das Instrument gewechselt werden. 
Wann und welches Instrument gespielt wird, bestimmt der Dirigent - und zwar möglichst so, dass der Zuschauer es nicht merkt. 
Sobald er den Dirigenten erkannt hat, kann er im Orchester mitmachen und der Dirigent verlässt den Raum. 
Für die zweite Runde wird ein neuer Dirigent ernannt.
