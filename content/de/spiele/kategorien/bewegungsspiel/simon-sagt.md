---
title: 'Simon Sagt'
description: "Mach genau, was Simon dir sagt, aber nur was Simon dir sagt."
"spiele/kategorien":
    - Alle Spiele
    - Bewegungsspiel
"spiele/zeitaufwand":
    - 5 - 10 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Zeit:** 5 - 10 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Ein Spieler übernimmt die Rolle des "Simons". 
Simon gibt den anderen Spielern Anweisungen, indem er sagt: "Simon sagt: Hände hoch!" 
Die Spieler müssen die Aktionen nur ausführen, wenn "Simon sagt" vor der Anweisung steht. 
Wenn Simon eine Anweisung gibt, ohne "Simon sagt" zu sagen, sollten die Spieler die Aktion ignorieren. 
Spieler, die dennoch die Aktion ausführen, scheiden aus. 
Das letzte verbleibende Kind wird der nächste "Simon".
