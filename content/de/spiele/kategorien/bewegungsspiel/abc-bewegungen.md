---
title: 'Abc Bewegungen'
description: "Die Kinder bewegen sich zum Alphabet."
"spiele/kategorien":
    - Alle Spiele
    - Bewegungsspiel
"spiele/zeitaufwand":
    - 5 - 10 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Zeit:** 5 - 10 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Die Spieler bewegen sich im Raum und beginnen mit dem Alphabet. 
Jeder Spieler muss sich fortbewegen und gleichzeitig ein Wort sagen, das mit dem Buchstaben beginnt, an dem er gerade ist. 
Zum Beispiel: "A" für Arme schwingen, "B" für Beine hoch heben usw. 
Das Spiel kann die Konzentration und das Bewusstsein für den eigenen Körper fördern.
