---
title: 'Evolutionsspiel'
description: "Schere, Stein, Papier, um die Evolutionsleiter zu erklimmen."
"spiele/kategorien":
    - Alle Spiele
    - Bewegungsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 5 - 10 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Zeit:** Eine Minute

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Am Anfang sind alle Affen. 
Während man ein Affe ist, verhält man sich auch so. 

Dann spielt man gegen dieselbe Evolutionsstufe "Schere, Stein, Papier" auf drei.  
Wer gewinnt, entwickelt sich weiter und wird zu einem Menschen und verhält sich so.  

Dann spielt man gegen dieselbe Evolutionsstufe "Schere, Stein, Papier" auf drei. 
Wenn man gewinnt, steigt man auf zum Cyborg (Roboter), wenn man aber verliert, wird man wieder ein Affe. 

Dann spielt man gegen dieselbe Evolutionsstufe "Schere, Stein, Papier" auf drei. 
Gewinnt man, hat man gewonnen, verliert man, wird man wieder zum Menschen. 

Fertig ist das Spiel erst, wenn alle die Evolutionsleiter emporgeklettert sind.
