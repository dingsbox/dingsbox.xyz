---
title: 'Super Mega High Five'
description: "Hier eine kurze Beschreibung des Spieles einfügen."
"spiele/kategorien":
    - Alle Spiele
    - Bewegungsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - < 5 min
"spiele/gruppengrösse":
    - 2 Personen
---

**Zeit:** 5 Minuten

**Gruppengrösse:** In 2er-Gruppen

---

**Spielbeschreibung:**

Die Spieler stellen sich zu zweit gegenüber auf.
Dann beginnen beide damit, zuerst zwei mal auf die Oberschenkel zu patschen.
Nun geht es im Takt weiter und die Spieler zeigen mit den Armen nach oben, unten, links oder rechts.
Zeigen die Spieler in die gleiche Richtung klatschen sie sich ein "High Five" und patschen anschliessend zwei mal.
Zeigen sie nicht in die gleiche Richtung wird nur gepatscht.
