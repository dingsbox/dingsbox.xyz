---
title: 'Peter Ruft Paul'
description: "Reagiere auf einen neuen Namen ohne den Takt zu verlieren."
"spiele/kategorien":
    - Alle Spiele
    - Bewegungsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 5 - 10 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Zeit:** Eine Minute

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Die Klasse sitzt im Kreis. 
Die Namen Peter und Paul werden an 2 Schüler vergeben. 
Alle übrigen Schüler bekommen eine Zahl zugeordnet. 
Die Schüler klatschen im Takt. 
«Peter» beginnt und ruft im Takt z.B. «Peter ruft 1», «1» übernimmt und ruft weiter «1 ruft Paul». 
Danach übernimmt «Paul». 
Wer aus dem Takt fällt oder einen Fehler macht, muss z. B. fünfmal einen Hampelmann machen.
