---
title: 'Zehnerli Hüpfen'
description: "Hüpfe tief und hoch, bis du die 10 erreichst."
"spiele/kategorien":
    - Alle Spiele
    - Bewegungsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 5 - 10 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Zeit:** 5 - 10 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Stellt euch hinter euren Stuhl und hüpft beidbeinig an Ort. 
Hüpft immer 5-mal tief und anschliessend hoch. 
Das erste Mal springt ihr 1-mal hoch, das zweite Mal springt ihr 2-mal hoch, das dritte Mal 3-mal etc. 

Fahrt so weiter, bis ihr zum Schluss 10-mal einen hohen Sprung ausgeführt habt.
