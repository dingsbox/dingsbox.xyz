---
title: 'Bewegungsspiele'
---

Unser Gehirn wird durch Bewegung aktiviert. 
Grundsätzlich bewegen sich Kinder heutzutage teilweise zu wenig. 
Verbringen meist den Unterricht sowie die Freizeit im Sitzen. 
Wenn wir uns bewegen, wird das Gehirn mit Sauerstoff und Energie versorgt, was uns im Endeffekt leistungsfähiger macht. 
Deshalb sollen Bewegungsspiele in den Unterricht unbedingt integriert werden, wenn die Kinder lange Zeit am Platz arbeiten. 
Bewegungsspiele können zudem die koordinativen Fähigkeiten oder auch das Gleichgewicht fördern. 
Das Ziel ist einerseits den SuS Freude an der Bewegung zu vermitteln andererseits dient es den Leistungen in der Schule.
