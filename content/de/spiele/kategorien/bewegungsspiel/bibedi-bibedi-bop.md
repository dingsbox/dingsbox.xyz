---
title: 'Bibedi Bibedi Bop'
description: "Ein Spiel in dem Reaktionsgeschwindigkeit und Konzentration auf die Probe gestellt werden."
"spiele/kategorien":
    - Alle Spiele
    - Bewegungsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 5 - 10 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Zeit:** Eine Minute

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Steht in einem Kreis und schliesst dabei einen Spieler ein. 
Dieser muss nun versuchen, aus dem Kreis herauszukommen, indem er die Personen, die den Kreis bilden, auf dem «falschen Bein» erwischt. 
Wer im Kreis nicht oder zu langsam reagiert, geht in die Mitte.

Der Spieler innerhalb des Kreises hat mehrere Möglichkeiten: 

Er kann sich vor eine Person aus dem Kreis stellen und «bibedi-bibedi-bop» sagen. 
Wenn diese Person nicht vor ihm «bop» sagt, muss sie in die Mitte. 
Der Spieler in der Mitte kann aber auch nur «bop» sagen. 
Sagt die angesprochene Person voreilig «bop», muss sie in die Mitte des Kreises gehen. 
Weiter kann er eine Person ansprechen und eine Figur verlangen, dabei müssen v. a. die Nachbarn des Angesprochenen aktiv werden. 

Einige Beispiele:

- Toaster: Der angesprochene Spieler hüpft auf und ab, die Nachbarn des Angesprochenen müssen sofort ihre Arme seitwärts ausstrecken, dabei den Toast einschliessen und stehenbleiben.
- Känguruh: Der angesprochene Spieler deutet mit seinen Armen einen Beutel vor seinem Bauch an, die Nachbarn des Angesprochenen strecken als Junge ihre Köpfe von unten nach oben aus dem Beutel heraus.
- Laterne: Der Spieler in der Mitte bleibt vor einer Person geradestehen, die Nachbarn des Angesprochenen heben als Hunde ein Bein Richtung Laterne und pinkeln sie pantomimisch an.
