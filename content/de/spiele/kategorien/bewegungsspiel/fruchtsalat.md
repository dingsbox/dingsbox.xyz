---
title: 'Fruchtsalat'
description: "Suche dir schnellstmöglich einen neuen Platz, wenn deine Obstsorte aufgerufen wird."
"spiele/kategorien":
    - Alle Spiele
    - Bewegungsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 5 - 10 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Material:** Stuhkreis

**Zeit:** 5 - 10 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Die Stühle werden im Kreis aufgestellt. 
Jedes Kind setzt sich auf einen Stuhl und bekommt eine Obstsorte zugeteilt. 
Dabei erhalten immer mehrere Kinder dieselbe Obstsorte (Apfel, Banane, Orange, Pflaume usw.). 
Danach wird einer der Stühle entfernt und ein Kind bleibt stehen. 
Das Kind ohne Stuhl darf nun die Obstsorte rufen und die gerufenen suche sich einen neuen Platz. 
Es spielt dann auch selbst mit und sucht sich einen Stuhl.  

Das Kind, welches keinen Stuhl findet, ruft die nächste Obstsorte. 

Die Kinder dürfen sich nicht wieder auf den eigenen Stuhl setzen, es muss ein neuer Stuhl sein. 

Wenn die Person in der Mitte "Obstsalat!" ruft, springen alle Kinder auf und suchen sich einen anderen Stuhl. 

Die Person kann auch mehr als eine Obstsorte rufen. 
Zum Beispiel ruft er: "Pflaume und Apfel!". 
Dann müssen alle Kinder der Gruppen Pflaume und Apfel die Plätze tauschen.
