---
title: 'Jukebox'
description: "Setze dich so schnell als möglich hin."
"spiele/kategorien":
    - Alle Spiele
    - Bewegungsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 5 - 10 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Material:** Musik

**Zeit:** 5 - 10 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Die SuS tanzen zur Musik. 
Sobald die Musik stoppt, müssen sie so schnell wie möglich auf den Boden sitzen. 
Das Kind, welches das langsamste war scheidet aus. 
Nun wird die Musik wieder eingeschaltet.

Das Spiel geht so lange weiter, bis nur noch ein Kind übrig ist.
