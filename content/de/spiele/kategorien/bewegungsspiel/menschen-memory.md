---
title: 'Menschen Memory'
description: "Finde je zwei Personen, die die gleiche Bewegung machen."
"spiele/kategorien":
    - Alle Spiele
    - Bewegungsspiel
"spiele/zeitaufwand":
    - 10 - 15 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Material:** Etwas

**Zeit:** 10 - 15 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Zwei Kinder gehen vor die Tür, die anderen finden sich beliebig zu 2er-Teams zusammen und überlegen sich eine Bewegung, welches sie vormachen. 
Dann verteilen sich die Kinder im Klassenraum – Verstecken verboten!

Die beiden Kinder werden hereingeholt und müssen die 2er-Teams finden. 
Dafür tippen sie Kinder an, die ihre Bewegung vorführen. 
Haben sie zwei Kinder gefunden, die das Gleiche tun, dann tippen sie diese zur Kontrolle gleichzeitig an. 
Die bereits erratenen 2er-Team setzen sich dann an ihre Plätze. 
Das 2er-Team, welches bis zuletzt übrigbleibt, darf nun vor die Tür gehen und eine neue Runde beginnt.
