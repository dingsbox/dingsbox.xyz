---
title: 'Reise Nach Jerusalem'
description: "Kinder gehen um den Stuhlkreis, sobald die Musik stoppt, setzen sich alle auf einen Stuhl."
"spiele/kategorien":
    - Alle Spiele
    - Bewegungsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 5 - 10 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Material:** Stühle (einer weniger als die Anzahl Spieler)

**Zeit:** 5 - 10 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Stelle die Stühle im Kreis auf und lasse die Spieler um die Stühle herumgehen, während Musik spielt. 
Wenn die Musik stoppt, müssen sich alle Spieler so schnell wie möglich auf einen freien Stuhl setzen. 
Der Spieler, der keinen Platz findet, scheidet aus. 
Entferne nach jeder Runde einen Stuhl, bis ein Gewinner übrigbleibt.
