---
title: 'Blinzelspiel'
description: "Ein Reaktionsspiel, bei dem viel geblinzelt wird."
"spiele/kategorien":
    - Alle Spiele
    - Bewegungsspiel
"spiele/tags":
"spiele/zeitaufwand":
    - 5 - 10 min
"spiele/gruppengrösse":
    - alle zusammen
---

**Zeit:** 5 - 10 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Die Kinder stellen sich im Kreis auf. 
Immer zwei Kinder stellen sich gemeinsam auf. 
Ein Kind steht vor das andere Kind mit ca. einem Schritt Abstand. 
Das hintere Kind hat die Arme hinter dem Rücken. 
Im Kreis steht ebenfalls ein Kind alleine. 
Sein Ziel ist es, eines der Kinder zu sich zu blinzeln. 
Indem das Kind jeweils die vorderen Kinder anblinzelt. 
Das Ziel der hinteren Kinder ist es ihr vorderes nicht gehen zu lassen. 
Wenn sie merken, dass das vordere Kind angeblinzelt wird, müssen sie es schnell mit beiden Armen packen, bevor es zum Blinzelkind davonläuft. 

Sobald es das Blinzelkind geschafft hat, ein vorderes Kind zu erblinzeln, gibt es ein neues Blinzelkind. 
Das Kind, welches sein vorderes Kind gehen gelassen hat, muss nun wiederum versuchen, sich ein Kind zu erblinzeln.
