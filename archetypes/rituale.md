---
title: '{{ replace .File.ContentBaseName `-` ` ` | title }}'
description: "Hier eine kurze Beschreibung des Rituals einfügen."
"rituale/kategorien":
    - Alle Rituale
    - Lebenszeitgliederung
    - Arbeitsstruktur
    - Zusammenleben
"rituale/tags":
    - z.B. Spezieller Anlass
"rituale/zeitaufwand":
    - < 5 min
    - 5 - 10 min
---

**Material:** (oder hier, falls nur ein Gegenstand)

- erster Gegenstand
- zweiter Gegenstand

**Zeit:** 10 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Hier könnte die Beschreibung deines Rituals stehen.
