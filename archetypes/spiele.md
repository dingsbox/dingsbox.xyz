---
title: '{{ replace .File.ContentBaseName `-` ` ` | title }}'
description: "Hier eine kurze Beschreibung des Spieles einfügen."
"spiele/kategorien":
    - Alle Spiele
    - Bewegungsspiel
    - Konzentrationsspiel
    - Kooperationsspiel
    - Kennenlernspiel
"spiele/tags":
    - z.B. Geschicklichkeit
"spiele/zeitaufwand":
    - < 5 min
    - 5 - 10 min
"spiele/gruppengrösse":
    - alle zusammen
    - 2 Personen
    - 4 - 5 Personen
---

**Material:** (oder hier, falls nur ein Gegenstand)

- erster Gegenstand
- zweiter Gegenstand

**Zeit:** 10 Minuten

**Gruppengrösse:** Alle zusammen

---

**Spielbeschreibung:**

Hier könnte die Beschreibung deines Spieles stehen.
